import 'package:flutter/material.dart';
import 'main.dart';
import 'package:line_icons/line_icons.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'dart:async';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:auto_size_text/auto_size_text.dart';
import './misc/AppData.dart';

// Quick Diagnostic screen
class QuickDiagnostic extends StatefulWidget {
  @override
  _QuickDiagnosticState createState() => new _QuickDiagnosticState();
}

// Quick Diagnostic screen design
class _QuickDiagnosticState extends State<QuickDiagnostic> {
  File pickedImage;
  String _barcode = 'Grideye ID';
  bool isImageLoaded = false;

  // scanning barcode fonction
  Future _getImageAndDecode() async {
    try{
    // get image file
    pickedImage = await ImagePicker.pickImage(source: ImageSource.camera);
    
    // create vision image from that file
    FirebaseVisionImage ourImage = FirebaseVisionImage.fromFile(pickedImage);

    // create detector index
    BarcodeDetector barcodeDetector = FirebaseVision.instance.barcodeDetector();
    
    // find barcode in image
    List barCodes = await barcodeDetector.detectInImage(ourImage);
    
    for (Barcode readableCode in barCodes) {
      setState(() {
         appData.barcode = readableCode.displayValue;
        _barcode = appData.barcode;
      });
    }
    }on FormatException{
      setState(() => _barcode = 'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => _barcode  = 'Unknown error. Please try again!');
    }

  }

  @override
  Widget build(BuildContext context) {
    return new AspectRatio(
        aspectRatio: 100 / 100,
        child: new Scaffold(
            appBar: new AppBar(
              elevation: 0.0,
              backgroundColor: greyApp.withOpacity(0),
              title: new Center(
                  child: new Container(
                      padding: EdgeInsets.only(right: 60.0),
                      child: new Text('Quick Diagnostic',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 23)))),
            ),
            backgroundColor: greyApp,
            body: new Theme(
                data: new ThemeData(
                  primaryColor: greenApp,
                  hintColor: greenApp,
                  textTheme: new TextTheme(
                    subhead: new TextStyle(color: Colors.white),
                  ),
                  scaffoldBackgroundColor: greyApp,
                  canvasColor: greyApp,
                  backgroundColor: greyApp.withOpacity(0),
                ),
                child: new Container(
                  padding: new EdgeInsets.all(32.0),
                  child: new Center(
                    child: new Column(
                      children: <Widget>[
                        new Row(
                          children: <Widget>[
                            new Container(
                              padding: new EdgeInsets.all(5.0),
                            ),
                          ],
                        ),
                        new GestureDetector(
                          onTap: () {
                            _getImageAndDecode();
                          },
                          child: new Container(
                            width: MediaQuery.of(context).size.height / 3.2,
                            height: MediaQuery.of(context).size.height / 3.2,
                            child: new FloatingActionButton.extended(
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.all(
                                      new Radius.circular(
                                          MediaQuery.of(context).size.height /
                                              3.2)),
                                  side: new BorderSide(
                                      color: greyCircle, width: 15.0)),
                              onPressed: null,
                              elevation: 10,
                              tooltip: 'Scan barcode',
                              label: new AutoSizeText('SCAN BARCODE',
                                  style: new TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12)),
                              backgroundColor: greenCircle,
                              icon: new Icon(LineIcons.barcode,
                                  size: 100, color: Colors.white),
                            ),
                          ),
                        ),
                        new Padding(padding: new EdgeInsets.all(16.0)),
                        new Container(
                          child: new AutoSizeText(_barcode,
                              style: new TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15)),
                        ),
                        new Padding(padding: new EdgeInsets.all(16.0)),
                        new ConstrainedBox(
                          constraints: new BoxConstraints.expand(
                              height: MediaQuery.of(context).size.height / 3.2),
                          child: new Card(
                            color: greyApp,
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(15.0),
                            ),
                            child: new Container(
                              decoration: new BoxDecoration(
                                  color: greenApp,
                                  borderRadius: new BorderRadius.all(
                                      Radius.circular(18))),
                              child: new Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  new Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      new Container(
                                        padding: new EdgeInsets.all(10.0),
                                        child: new AutoSizeText('GSM Status',
                                            style: new TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13)),
                                      ),
                                      new Container(
                                          padding: new EdgeInsets.all(10.0),
                                          child: new CircleAvatar(
                                              backgroundColor: greyApp,
                                              child: new LayoutBuilder(builder:
                                                  (context, constraint) {
                                                return new Icon(Icons.check,
                                                    size: constraint
                                                            .biggest.height /
                                                        2,
                                                    color: Colors.white);
                                              })))
                                    ],
                                  ),
                                  new Divider(
                                    color: Colors.white,
                                  ),
                                  new Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      new Container(
                                        padding: new EdgeInsets.all(10.0),
                                        child: new AutoSizeText('GPS Status',
                                            style: new TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13)),
                                      ),
                                      new Container(
                                        padding: new EdgeInsets.all(10.0),
                                        child: new CircleAvatar(
                                            backgroundColor: greyApp,
                                            child: new LayoutBuilder(
                                                builder: (context, constraint) {
                                              return new Icon(Icons.close,
                                                  size: constraint
                                                          .biggest.height /
                                                      2,
                                                  color: Colors.white);
                                            })),
                                      )
                                    ],
                                  ),
                                  new Divider(
                                    color: Colors.white,
                                  ),
                                  new Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      new Container(
                                        padding: new EdgeInsets.all(10.0),
                                        child: new AutoSizeText('Number of SUR',
                                            style: new TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13)),
                                      ),
                                      new Container(
                                        padding: new EdgeInsets.all(10.0),
                                        child: new CircleAvatar(
                                          backgroundColor: greyApp,
                                          child: new AutoSizeText(
                                            '3',
                                            style: new TextStyle(
                                                color: Colors.white),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ))));
  }
}
