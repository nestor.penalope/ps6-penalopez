import 'package:flutter/material.dart';
import 'main.dart';
import './misc/Animated.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import './misc/AppData.dart';

// Add Grideye Screen
class AddGridEye extends StatefulWidget {
  @override
  _AddGridEyeState createState() => new _AddGridEyeState();
}

// SUR list values
enum SUR {
  SUR1_1,
  SUR1_2,
  SUR1_3,
  SUR1_4,
  SUR2_1,
  SUR2_2,
  SUR2_3,
  SUR2_4,
  SUR3_1,
  SUR3_2,
  SUR3_3,
  SUR3_4
}

// Add Grideye screen design
class _AddGridEyeState extends State<AddGridEye> {
  /* * * * * * * * * 
   * 
   * Stepper logic
   * 
   * * * * * * * * */
  int _stepCounter = 0;
  List<Step> _mySteps() {
    List<Step> _steps = [
      // first step "Electrical work"
      Step(
        title: new Text(
          "Electrical\nWork",
          textAlign: TextAlign.center,
          style:
              new TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        ),
        content: new Container(
          child: new Column(
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new TextFormField(
                      controller: _electricalWork,
                      maxLines: 1,
                      maxLength: 40,
                      decoration: new InputDecoration(
                          labelText: 'Electrical Work Name',
                          labelStyle: new TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: greenApp),
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0))),
                      style: new TextStyle(color: Colors.white),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  new Container(
                    padding: new EdgeInsets.all(10.0),
                  ),
                  new GestureDetector(
                      onTap: () {
                        _getElectricalWorkName();
                      },
                      child: new Container(
                          padding: new EdgeInsets.only(bottom: 25.0),
                          child: new Animated(Icons.center_focus_strong)))
                ],
              ),
              new Padding(padding: new EdgeInsets.all(16.0)),
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new TextFormField(
                      controller: _descriptionElectricalWork,
                      maxLines: 2,
                      maxLength: 50,
                      decoration: new InputDecoration(
                          labelText: 'Description',
                          labelStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: greenApp),
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0))),
                      style: new TextStyle(color: Colors.white),
                      keyboardType: TextInputType.text,
                    ),
                  )
                ],
              ),
              new Padding(padding: new EdgeInsets.all(16.0)),
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new DropdownButtonFormField(
                      decoration: new InputDecoration(
                        labelText: 'Asset Type',
                        labelStyle: new TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: greenApp),
                        border: new OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                      ),
                      value: _currentAssetType,
                      items: _dropDownMenuAssetType,
                      onChanged: changedDropDownAssetType,
                    ),
                  )
                ],
              ),
              new Padding(padding: new EdgeInsets.all(16.0)),
              new Row(
                children: <Widget>[
                  new Icon(
                    Icons.location_on,
                    color: greenApp,
                  ),
                  new Expanded(
                      child: new Text(' Current location',
                          style: new TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: greenApp)))
                ],
              ),
              new Padding(padding: new EdgeInsets.all(16.0)),
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(30.0),
                      child: new Container(
                          height: 150.0,
                          child: new Center(
                              child: new GoogleMap(
                            markers: Set<Marker>.of(markers.values),
                            initialCameraPosition: _googleMaps,
                            onMapCreated: (GoogleMapController controller) {
                              _onMapCreated();
                            },
                          ))),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
        isActive: _stepCounter >= 0,
      ),
      // second step "Attribute definition"
      Step(
        title: new Text(
          "Attribute\nDefinition",
          textAlign: TextAlign.center,
          style:
              new TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        ),
        content: new Container(
          child: new Column(
            children: <Widget>[
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new ButtonTheme(
                      buttonColor: greenApp,
                      height: 50.0,
                      child: new RaisedButton.icon(
                        icon: new Animated(Icons.center_focus_strong),
                        shape: new RoundedRectangleBorder(
                          side: new BorderSide(
                            color: new Color.fromRGBO(118, 171, 116, 1.0),
                            width: 1.0,
                            style: BorderStyle.solid,
                          ),
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                        onPressed: _getAttributeDefinition,
                        label: new Text("Scan parameters from plate",
                            style: new TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                      ))
                ],
              ),
              new Padding(padding: new EdgeInsets.all(16.0)),
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new TextFormField(
                      controller: _attributeName,
                      maxLines: 1,
                      maxLength: 25,
                      decoration: new InputDecoration(
                          labelText: 'Attribute Name',
                          labelStyle: new TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: greenApp),
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0))),
                      style: new TextStyle(color: Colors.white),
                      keyboardType: TextInputType.text,
                    ),
                  )
                ],
              ),
              new Padding(padding: new EdgeInsets.all(16.0)),
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new TextFormField(
                      maxLines: 2,
                      maxLength: 50,
                      controller: _attributeDescription,
                      decoration: new InputDecoration(
                          labelText: 'Description',
                          labelStyle: new TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: greenApp),
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0))),
                      style: new TextStyle(color: Colors.white),
                      keyboardType: TextInputType.text,
                    ),
                  )
                ],
              ),
              new Padding(padding: new EdgeInsets.all(16.0)),
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new TextFormField(
                      maxLines: 1,
                      maxLength: 6,
                      controller: _nominalPower,
                      decoration: new InputDecoration(
                          labelText: 'Nominal Power [kvA]',
                          labelStyle: new TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: greenApp),
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0))),
                      style: new TextStyle(color: Colors.white),
                      keyboardType: TextInputType.number,
                    ),
                  )
                ],
              ),
              new Padding(padding: new EdgeInsets.all(16.0)),
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new TextFormField(
                      maxLines: 1,
                      maxLength: 6,
                      controller: _operatingMV,
                      decoration: new InputDecoration(
                          labelText: 'Operating MV voltage [kV]',
                          labelStyle: new TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: greenApp),
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0))),
                      style: new TextStyle(color: Colors.white),
                      keyboardType: TextInputType.number,
                    ),
                  )
                ],
              ),
              new Padding(padding: new EdgeInsets.all(16.0)),
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new TextFormField(
                      maxLines: 1,
                      maxLength: 6,
                      controller: _operatingLV,
                      decoration: new InputDecoration(
                          labelText: 'Operating LV voltage [V]',
                          labelStyle: new TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: greenApp),
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0))),
                      style: new TextStyle(color: Colors.white),
                      keyboardType: TextInputType.number,
                    ),
                  )
                ],
              ),
              new Padding(padding: new EdgeInsets.all(16.0)),
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new TextFormField(
                      maxLines: 1,
                      maxLength: 6,
                      controller: _impedance,
                      decoration: new InputDecoration(
                          labelText: 'Impedance voltage [%]',
                          labelStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: greenApp),
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0))),
                      style: new TextStyle(color: Colors.white),
                      keyboardType: TextInputType.number,
                    ),
                  )
                ],
              ),
              new Padding(padding: new EdgeInsets.all(16.0)),
              new Row(
                children: <Widget>[
                  new Expanded(
                      child: new CheckboxListTile(
                    value: _mvVoltage,
                    onChanged: _onValueChange,
                    title: new Text(
                      'MV Voltage estimation',
                      style: new TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: _color),
                    ),
                    controlAffinity: ListTileControlAffinity.trailing,
                    activeColor: greenApp,
                  ))
                ],
              ),
              new Padding(padding: new EdgeInsets.all(16.0)),
              _mvVoltage
                  ? new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new TextFormField(
                            maxLines: 1,
                            maxLength: 6,
                            controller: _tapPosition,
                            decoration: new InputDecoration(
                                labelText: 'Tap Position MV voltage [kV]',
                                labelStyle: new TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: greenApp),
                                border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(20.0))),
                            style: new TextStyle(color: Colors.white),
                            keyboardType: TextInputType.number,
                          ),
                        )
                      ],
                    )
                  : new Container(),
              _mvVoltage
                  ? new Row(
                      children: <Widget>[
                        new Padding(padding: new EdgeInsets.all(16.0)),
                      ],
                    )
                  : new Container(),
              _mvVoltage
                  ? new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new DropdownButtonFormField(
                            decoration: new InputDecoration(
                              labelText: 'Vector Group',
                              labelStyle: new TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: greenApp),
                              border: new OutlineInputBorder(
                                  borderRadius:
                                      new BorderRadius.circular(20.0)),
                            ),
                            value: _currentVectorGroup,
                            items: _dropDownMenuVectorGroup,
                            onChanged: changedDropDownVectorGroup,
                          ),
                        )
                      ],
                    )
                  : new Container(),
              _mvVoltage
                  ? new Padding(padding: new EdgeInsets.all(16.0))
                  : new Container()
            ],
          ),
        ),
        isActive: _stepCounter >= 1,
      ),
      // third step "Current Mapping"
      Step(
        title: new Text(
          "Current\nMapping",
          textAlign: TextAlign.center,
          style:
              new TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        ),
        content: new Container(
            child: new Column(
          children: <Widget>[
            new Row(
              children: <Widget>[
                new Expanded(
                    child: new Text('Link current line with reference voltage',
                        style: new TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.white)))
              ],
            ),
            new Padding(padding: new EdgeInsets.all(16.0)),
            new Row(
              children: <Widget>[
                new Expanded(
                  child: new Text(_textIL1,
                      style: new TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: greenApp)),
                ),
                new Padding(padding: new EdgeInsets.all(16.0)),
                new PopupMenuButton<SUR>(
                    child: new Icon(
                      Icons.input,
                      color: Colors.white,
                    ),
                    initialValue: _selectedSUR,
                    onSelected: _onSelectedS1,
                    itemBuilder: (BuildContext context) {
                      return _items;
                    }),
              ],
            ),
            new Row(
              children: <Widget>[
                new Container(
                  padding: new EdgeInsets.all(16.0),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Expanded(
                  child: new Text(
                    _textIL2,
                    style: new TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: greenApp),
                  ),
                ),
                new Padding(padding: new EdgeInsets.all(16.0)),
                new PopupMenuButton<SUR>(
                    child: new Icon(
                      Icons.input,
                      color: Colors.white,
                    ),
                    initialValue: _selectedSUR,
                    onSelected: _onSelectedS2,
                    itemBuilder: (BuildContext context) {
                      return _items;
                    }),
              ],
            ),
            new Padding(padding: new EdgeInsets.all(16.0)),
            new Row(
              children: <Widget>[
                new Expanded(
                  child: new Text(
                    _textIL3,
                    style: new TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: greenApp),
                  ),
                ),
                new Padding(padding: new EdgeInsets.all(16.0)),
                new PopupMenuButton<SUR>(
                    child: new Icon(
                      Icons.input,
                      color: Colors.white,
                    ),
                    initialValue: _selectedSUR,
                    onSelected: _onSelectedS3,
                    itemBuilder: (BuildContext context) {
                      return _items;
                    })
              ],
            ),
            new Padding(padding: new EdgeInsets.all(16.0)),
          ],
        )),
        isActive: _stepCounter >= 2,
      ),
      // final step "Bubble selection"
      Step(
        title: new Text(
          "Bubble\nSelection",
          textAlign: TextAlign.center,
          style:
              new TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        ),
        content: new Container(child: makeRadioTiles()),
        isActive: _stepCounter >= 3,
      ),
    ];
    return _steps;
  }

  /* * * * * * * * * 
   * 
   * Electrical work
   * 
   * * * * * * * * */
  //google maps controller
  GoogleMapController mapController;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  MarkerId selectedMarker;

  // creates the map and places the marker
  void _onMapCreated() {
    final String markerIdVal = '1';
    final MarkerId markerId = MarkerId(markerIdVal);
    final Marker marker = new Marker(
        markerId: markerId,
        icon: BitmapDescriptor.fromAsset('assets/images/marker.png'),
        position: new LatLng(
          46.8060,
          7.1620,
        ),
        infoWindow: new InfoWindow(title: 'Grideye Location'));

    setState(() {
      // adding a new marker to map
      markers[markerId] = marker;
    });
  }

  //electrical work name text controller
  var _electricalWork = new TextEditingController();

  //electrical work description text controller
  var _descriptionElectricalWork = new TextEditingController();

  // OCR fonction
  void _getElectricalWorkName() async {
    // try and catch to treats errors
    try {
      // get image file
      final imageFile = await ImagePicker.pickImage(source: ImageSource.camera);

      // create vision image from that file
      final FirebaseVisionImage visionImage =
          FirebaseVisionImage.fromFile(imageFile);

      // create detector index
      final TextRecognizer textRecognizer =
          FirebaseVision.instance.textRecognizer();

      // find text in image
      final VisionText visionText =
          await textRecognizer.processImage(visionImage);

      // in case no text is found this will show up instead
      String text = 'Please scan again';

      // loops trough all the text to get the Electrical work name
      for (TextBlock block in visionText.blocks) {
        text = block.text.toString().replaceAll('\n', ' ');
      }

      // update the Electrical work name value
      if (this.mounted) {
        setState(() {
          _electricalWork.text = text;
        });
      }
    } //if something is wrong alert a message with the error
    catch (e) {
      _ackAlert(context, e);
    }
  }

  //Alert box to show errors
  Future<void> _ackAlert(BuildContext context, e) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(24.0))),
          backgroundColor: greenApp,
          title: new Center(
              child: new Text('Something went wrong',
                  style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Colors.white))),
          content: new Container(
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: new BorderRadius.all(new Radius.circular(32.0)),
            ),
            child: Text('Unknown error: $e',
                style:
                    new TextStyle(color: greyApp, fontWeight: FontWeight.bold)),
            constraints: BoxConstraints(
              minWidth: 400,
              maxHeight: 750.0,
              maxWidth: 600.0,
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Ok',
                style: new TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  // values initialisation for asset types
  List<DropdownMenuItem<String>> _dropDownMenuAssetType;
  String _currentAssetType;
  List _assetTypes = [
    "Transformer Station",
    "Distribution Cabinet",
    "Other",
    "Private"
  ];

  // marker position
  static final CameraPosition _googleMaps =
      CameraPosition(target: LatLng(46.8065, 7.1620), zoom: 14.4746);

  //creates the assets value dropdown list
  List<DropdownMenuItem<String>> getDropDownMenuAssetType() {
    List<DropdownMenuItem<String>> items = new List();
    for (String type in _assetTypes) {
      items.add(new DropdownMenuItem(value: type, child: new Text(type)));
    }
    return items;
  }

  // updates the asset type value
  void changedDropDownAssetType(String seletedType) {
    setState(() {
      _currentAssetType = seletedType;
    });
  }

  /* * * * * * * * * 
   * 
   * Attribute definition
   * 
   * * * * * * * * */

  // declare the group vector's list
  List<DropdownMenuItem<String>> _dropDownMenuVectorGroup;

  //Vector group values
  String _currentVectorGroup;
  List _vectorGroups = ["Dy5", "Dy11", "Dy1", "Yy0", "Yz5", "Yz11"];

  //MV voltage estimation value
  bool _mvVoltage = false;

  // declare and initiliaze the text color of "MV Voltage estimation"
  Color _color = greenApp;

  // changes color of "MV Voltage estimation" if active or not
  void _onValueChange(bool mvVoltage) {
    setState(() {
      _mvVoltage = mvVoltage;
      if (mvVoltage) _color = Colors.white;
      if (!mvVoltage) _color = greenApp;
    });
  }

  // formular controllers
  var _attributeName = new TextEditingController();
  var _attributeDescription = new TextEditingController();
  var _nominalPower = new TextEditingController();
  var _operatingMV = new TextEditingController();
  var _operatingLV = new TextEditingController();
  var _impedance = new TextEditingController();
  var _tapPosition = new TextEditingController();

  // fonction called when "Scan parameters from plate" button is pressed
  void _getAttributeDefinition() async {
    // try and catch to treats errors
    try {
      // get image file
      final imageFile = await ImagePicker.pickImage(source: ImageSource.camera);

      // create vision image from that file
      final FirebaseVisionImage visionImage =
          FirebaseVisionImage.fromFile(imageFile);

      // create detector index
      final TextRecognizer textRecognizer =
          FirebaseVision.instance.textRecognizer();

      // find text in image
      final VisionText visionText =
          await textRecognizer.processImage(visionImage);

      // fiel's declaration and initialization
      String nominalPower = '';
      String impedance = '';
      String operatingMV = '';
      String operatingLV = '';

      // pattern that match everything after a "/" symbol
      String pattern = r"\/(.*)";
      RegExp regEx = RegExp(pattern);

      // loops trough the text to find corresponding values
      for (TextBlock block in visionText.blocks) {
        for (TextLine line in block.lines) {
          for (int i = 1; i < line.elements.length; i++) {
            TextElement element = line.elements[i];
            //finds nominal power value
            if (element.text.toLowerCase() == "kva" ||
                element.text.toLowerCase().contains("kva")) {
              var prevElement = line.elements[i - 1];
              nominalPower = prevElement.text;
            }
            //finds impedance value
            if (element.text.toLowerCase() == "uk") {
              var nextElement = line.elements[i + 1];
              impedance = nextElement.text.replaceAll('%', '');
            }
            if (element.text.contains("%")) {
              impedance = element.text.replaceAll('%', '');
            }
            if (element.text == "%") {
              var prevElement = line.elements[i - 1];
              impedance = prevElement.text;
            }
            //finds operating MV value
            if (element.text.toLowerCase() == "um") {
              var nextElement = line.elements[i + 1];
              operatingMV = nextElement.text.replaceAll(regEx, '');
            }
            if (element.text.toLowerCase().contains("kv") &&
                !element.text.toLowerCase().contains("kva")) {
              var prevElement = line.elements[i - 1];
              operatingMV = prevElement.text.replaceAll(regEx, '');
            }
            //finds operating LV value
            if (element.text.toLowerCase() == "v") {
              var prevElement = line.elements[i - 1];
              operatingLV = prevElement.text.replaceAll(regEx, '');
            }
          }
        }
      }
      //updates textfields values
      if (this.mounted) {
        setState(() {
          _attributeName.text = _currentAssetType;
          _nominalPower.text = nominalPower;
          _impedance.text = impedance;
          _operatingMV.text = operatingMV;
          _operatingLV.text = operatingLV;
          _tapPosition.text = "0";
        });
      }
    } //if something is wrong alert a message with the error
    catch (e) {
      _ackAlert(context, e);
    }
  }

  //creates the Vector group dropdown
  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String type in _vectorGroups) {
      items.add(new DropdownMenuItem(value: type, child: new Text(type)));
    }
    return items;
  }

  // updates the vector group value
  void changedDropDownVectorGroup(String seletedType) {
    setState(() {
      _currentVectorGroup = seletedType;
    });
  }

  /* * * * * * * * * 
   * 
   * Current Mapping
   * 
   * * * * * * * * */
  // Initializes all  fields values for the current mapping screen
  SUR _selectedSUR = SUR.SUR1_1;
  String _valueIL1, _valueIL2, _valueIL3;
  String _textIL1 = 'Select SUR for IL1';
  String _textIL2 = 'Select SUR for IL2';
  String _textIL3 = 'Select SUR for IL3';

  // creates the SUR's list
  List<PopupMenuEntry<SUR>> _items = new List<PopupMenuEntry<SUR>>();

  // updates the IL1 value
  void _onSelectedS1(SUR sur) {
    setState(() {
      _selectedSUR = sur;
      _valueIL1 = _getDisplay(sur);
      _textIL1 = 'IL1 is map to ${_getDisplay(sur)}';
    });
  }

  // updates the IL2 <value
  void _onSelectedS2(SUR sur) {
    setState(() {
      _selectedSUR = sur;
      _valueIL2 = _getDisplay(sur);
      _textIL2 = 'IL2 is map to ${_getDisplay(sur)}';
    });
  }

  // updates the IL3 <value
  void _onSelectedS3(SUR sur) {
    setState(() {
      _selectedSUR = sur;
      _valueIL3 = _getDisplay(sur);
      _textIL3 = 'IL3 is map to ${_getDisplay(sur)}';
    });
  }

  // display the message when an SUR is selected
  String _getDisplay(SUR sur) {
    int index = sur.toString().indexOf('.');
    index++;
    return sur.toString().substring(index);
  }

  /* * * * * * * * * 
   * 
   * Bubble selection
   * 
   * * * * * * * * */
  // declares and updates the select bubble
  int _selectedBubble = 0;
  void _setValue(int bubble) => setState(() => _selectedBubble = bubble);

  // creates the bubble's list
  Widget makeRadioTiles() {
    List<Widget> list = new List<Widget>();
    for (int i = 0; i < 3; i++) {
      list.add(new RadioListTile(
        value: i,
        groupValue: _selectedBubble,
        onChanged: _setValue,
        activeColor: greenApp,
        controlAffinity: ListTileControlAffinity.trailing,
        title: new Text(
          'Bubble ${i + 1}',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ));
    }

    Column column = new Column(children: list);
    return column;
  }

  /* * * * * * * * * 
   * 
   * HTTP requests
   * 
   * * * * * * * * */
  // authentification Thingworx server
  String basicAuth = 'Basic ' + base64Encode(utf8.encode('user1:user1'));

  // creates a new grideye
  Future<Map<String, dynamic>> postRequest() async {
    var body = jsonEncode({
      "bubble": "BUBBLE.DEPsys.1",
      "module": appData.barcode,
      "name": _electricalWork.text,
      "description": _descriptionElectricalWork.text,
      "gridAttributeType": "TransformerTT",
      "nodeName": _attributeName.text,
      "descNode": _attributeDescription.text,
      "location": {
        "latitude": 46.49737958614847,
        "longitude": 6.694111755534891
      },
      "paramList": {
        "rows": [
          {
            "operatingMvVoltage": _operatingMV.text,
            "operatingLvVoltage": _operatingLV.text,
            "ImpedanceVoltage": _impedance.text,
            "nomS": _nominalPower.text,
            "MVEstimated": _mvVoltage.toString(),
            "vectorGroup": _currentVectorGroup.toString(),
            "tap": _tapPosition.text
          }
        ],
        "dataShape": {
          "fieldDefinitions": {
            "operatingMvVoltage": {
              "name": "operatingMvVoltage",
              "aspects": {},
              "description": "",
              "baseType": "NUMBER",
              "ordinal": 0
            },
            "inverterType": {
              "name": "inverterType",
              "aspects": {},
              "description": "",
              "baseType": "STRING",
              "ordinal": 0
            },
            "vectorGroup": {
              "name": "vectorGroup",
              "aspects": {},
              "description": "",
              "baseType": "STRING",
              "ordinal": 0
            },
            "virtual": {
              "name": "virtual",
              "aspects": {},
              "description": "",
              "baseType": "BOOLEAN",
              "ordinal": 0
            },
            "tap": {
              "name": "tap",
              "aspects": {},
              "description": "",
              "baseType": "NUMBER",
              "ordinal": 0
            },
            "operatingLvVoltage": {
              "name": "operatingLvVoltage",
              "aspects": {},
              "description": "",
              "baseType": "NUMBER",
              "ordinal": 0
            },
            "nomI": {
              "name": "nomI",
              "aspects": {},
              "description": "",
              "baseType": "NUMBER",
              "ordinal": 0
            },
            "inverterNominalApparentPower": {
              "name": "inverterNominalApparentPower",
              "aspects": {},
              "description": "",
              "baseType": "STRING",
              "ordinal": 0
            },
            "MVEstimated": {
              "name": "MVEstimated",
              "aspects": {},
              "description": "",
              "baseType": "BOOLEAN",
              "ordinal": 0
            },
            "inverterPowerFactor": {
              "name": "inverterPowerFactor",
              "aspects": {},
              "description": "",
              "baseType": "STRING",
              "ordinal": 0
            },
            "nomIN": {
              "name": "nomIN",
              "aspects": {},
              "description": "",
              "baseType": "NUMBER",
              "ordinal": 0
            },
            "ImpedanceVoltage": {
              "name": "ImpedanceVoltage",
              "aspects": {},
              "description": "",
              "baseType": "NUMBER",
              "ordinal": 0
            },
            "nomS": {
              "name": "nomS",
              "aspects": {},
              "description": "",
              "baseType": "NUMBER",
              "ordinal": 0
            },
            "nomU": {
              "name": "nomU",
              "aspects": {},
              "description": "",
              "baseType": "NUMBER",
              "ordinal": 0
            }
          }
        }
      }
    });
    var response = await http.post(
        Uri.encodeFull(
            "http://192.168.43.174:8080/Thingworx/Things/Helper.DEPsys.SI.ElementManager/Services/CreateGridAttribute"),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          'authorization': basicAuth,
        },
        body: body);
    setState(() {
      var res = json.decode(response.body);
      print(res);
    });

    return json.decode(response.body);
  }

  // launch the request and navigate to the Dashboard screen when finished
  void _onSendFormular() {
    postRequest();
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AMA()),
    );
  }

  /* * * * * * * * * 
   * 
   * State initialisation
   * 
   * * * * * * * * */
  @override
  void initState() {
    super.initState();
    _mvVoltage = false;
    _dropDownMenuAssetType = getDropDownMenuAssetType();
    _currentAssetType = _dropDownMenuAssetType[0].value;
    _dropDownMenuVectorGroup = getDropDownMenuItems();
    _currentVectorGroup = _dropDownMenuVectorGroup[0].value;
    for (SUR sur in SUR.values) {
      _items.add(new PopupMenuItem(
        child: new Text(
          _getDisplay(sur),
        ),
        value: sur,
      ));
    }
  }

  /* * * * * * * * * 
   * 
   * Formular completed alert
   * 
   * * * * * * * * */
  // formular confirmation pop up
  void finalTest(BuildContext context) {
    var alertDialog = AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(24.0))),
      backgroundColor: greenApp,
      title: new Center(
          child: new Text('Form completed',
              style: new TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 18, color: greyApp))),
      content: new Container(
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: new BorderRadius.all(new Radius.circular(32.0)),
        ),
        constraints: BoxConstraints(
          minWidth: 400,
          maxHeight: 750.0,
          maxWidth: 600.0,
        ),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Padding(padding: new EdgeInsets.all(2.0)),
            new Divider(
              color: Colors.white,
            ),
            new Text('Electrical Work',
                style:
                    new TextStyle(color: greyApp, fontWeight: FontWeight.bold)),
            new Divider(
              color: Colors.white,
            ),
            new Text('Name: ${_electricalWork.text}',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Text('Desc.: ${_descriptionElectricalWork.text}',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Text('Asset Type: $_currentAssetType',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Padding(padding: new EdgeInsets.all(2.0)),
            new Divider(
              color: Colors.white,
            ),
            new Text('Attribute Definition',
                style:
                    new TextStyle(color: greyApp, fontWeight: FontWeight.bold)),
            new Divider(
              color: Colors.white,
            ),
            new Text('Name: ${_attributeName.text}',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Text('Desc.: ${_attributeDescription.text}',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Text('Nominal Power: ${_nominalPower.text}',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Text('Operating MV voltage: ${_operatingMV.text}',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Text('Operating LV voltage: ${_operatingLV.text}',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Text('Impedance voltage: ${_impedance.text}',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Text('MV Voltage estimation: $_mvVoltage',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Text('Tap Position MV voltage: ${_tapPosition.text}',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Text('Vector Group: $_currentVectorGroup',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Padding(padding: new EdgeInsets.all(2.0)),
            new Divider(
              color: Colors.white,
            ),
            new Text('Current Mapping',
                style:
                    new TextStyle(color: greyApp, fontWeight: FontWeight.bold)),
            new Divider(
              color: Colors.white,
            ),
            new Text('Selected SUR for IL1: $_valueIL1',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Text('Selected SUR for IL2: $_valueIL2',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Text('Selected SUR for IL3: $_valueIL3',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Padding(padding: new EdgeInsets.all(2.0)),
            new Divider(
              color: Colors.white,
            ),
            new Text('Bubble Selection',
                style:
                    new TextStyle(color: greyApp, fontWeight: FontWeight.bold)),
            new Divider(
              color: Colors.white,
            ),
            new Text('Selected Bubble: Bubble ${_selectedBubble + 1}',
                style: new TextStyle(color: Colors.white, fontSize: 12)),
            new Padding(padding: new EdgeInsets.all(10.0)),
            new ButtonTheme(
                buttonColor: greyApp,
                height: 50.0,
                child: new RaisedButton.icon(
                  icon: new Animated(Icons.send),
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                  ),
                  onPressed: _onSendFormular,
                  label: new Text("Send Request",
                      style: new TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold)),
                )),
          ],
        ),
      ),
    );
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alertDialog;
        });
  }

  /* * * * * * * * * 
   * 
   * Screen presentation
   * 
   * * * * * * * * */
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          elevation: 0.0,
          backgroundColor: greyApp.withOpacity(0),
          title: new Center(
              child: new Container(
                  padding: EdgeInsets.only(right: 60.0),
                  child: new Text('Add Grideye',
                      style: new TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 23)))),
        ),
        backgroundColor: greyApp,
        body: new Theme(
          data: new ThemeData(
            textTheme: new TextTheme(
              subhead: new TextStyle(color: Colors.white),
            ),
            primaryColor: greenApp,
            hintColor: greenApp,
            highlightColor: greyApp,
            cardColor: greyApp,
            scaffoldBackgroundColor: greyApp,
            canvasColor: greyApp,
            backgroundColor: greyApp.withOpacity(0),
          ),
          child: new Container(
              color: greyApp.withOpacity(0),
              child: new Stepper(
                currentStep: this._stepCounter,
                steps: _mySteps(),
                type: StepperType.horizontal,
                onStepTapped: (step) {
                  setState(() {
                    this._stepCounter = step;
                  });
                },
                onStepCancel: () {
                  setState(() {
                    this._stepCounter > 0
                        ? this._stepCounter -= 1
                        : this._stepCounter = 0;
                  });
                },
                onStepContinue: () {
                  setState(() {
                    if (this._stepCounter < _mySteps().length - 1) {
                      this._stepCounter += 1;
                    } else {
                      finalTest(context);
                    }
                  });
                },
              )),
        ));
  }
}
