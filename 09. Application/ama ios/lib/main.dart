import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'add.dart';
import 'diagnostic.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:splashscreen/splashscreen.dart';

// constant colors declaration
Color greenApp = const Color.fromRGBO(118, 171, 116, 1.0);
Color textColor = Colors.white;
Color greenCircle = const Color.fromRGBO(92, 114, 102, 1.0);
Color greyApp = const Color.fromRGBO(74, 81, 96, 1.0);
Color greyCircle = const Color.fromRGBO(80, 93, 99, 1.0);

// the application is launched from this method
void main() =>
    //the screen orientation is set to allways portrait
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
        .then((_) {
      runApp(new MaterialApp(
          title: 'AMA',
          debugShowCheckedModeBanner: false,
          home: new SplashAMA()));
    });

// Splash screen design
class SplashAMA extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
      seconds: 2,
      navigateAfterSeconds: new AMA(),
      title: new Text(
        'Assistance Mobile Application',
        style: new TextStyle(
            fontWeight: FontWeight.bold, fontSize: 20.0, color: textColor),
      ),
      image: new Image.asset('assets/launcher/foreground.png'),
      backgroundColor: greenApp,
      styleTextUnderTheLoader: new TextStyle(),
      photoSize: 100.0,
      loaderColor: greyApp,
    );
  }
}

// Dashboard screen design
class AMA extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return new AspectRatio(
        aspectRatio: 100 / 100,
        child: new Scaffold(
          appBar: new AppBar(
              elevation: 0.0,
              backgroundColor: greyApp,
              title: new Center(
                child: new Container(
                    child: new Text('Dashboard',
                        style: new TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 23))),
              )),
          backgroundColor: greyApp,
          body: Container(
            padding: EdgeInsets.all(32.0),
            child: Center(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        myCard(context, ' Quick\n Diagnostic   ',
                            QuickDiagnostic(), Icons.assignment_turned_in),
                        myCard(context, ' Add\n Grideye        ', AddGridEye(),
                            Icons.add_circle),
                        myCard(context, ' Grideye\n List               ', AMA(),
                            Icons.list),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}

// creates a card
// params: context, card title, link, icon
myCard(BuildContext context, String s, Widget a, IconData i) {
  return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => a),
        );
      },
      child: Container(
          constraints: BoxConstraints.expand(
              height: MediaQuery.of(context).size.height / 4),
          child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              margin: EdgeInsets.all(20.0),
              elevation: 5.0,
              color: greenApp,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Align(
                      alignment: Alignment.centerLeft,
                      child: new AutoSizeText(
                        s,
                        style: new TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 25),
                      )),
                  Align(
                      alignment: Alignment.centerRight,
                      child: new LayoutBuilder(builder: (context, constraint) {
                        return new Icon(i,
                            size: constraint.biggest.height / 2,
                            color: Colors.white);
                      })),
                ],
              ))));
}
