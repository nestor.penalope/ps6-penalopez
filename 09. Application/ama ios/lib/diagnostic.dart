import 'package:flutter/material.dart';
import 'main.dart';
import 'package:line_icons/line_icons.dart';
import 'dart:async';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import './misc/AppData.dart';

// Quick Diagnostic screen
class QuickDiagnostic extends StatefulWidget {
  @override
  _State createState() => new _State();
}

// Quick Diagnostic screen design
class _State extends State<QuickDiagnostic> {
  String barcode = "";

  // scanning barcode fonction
  Future _scan() async {
    //updates the barcode value 
    try {
      appData.barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = appData.barcode);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException{
      setState(() => this.barcode = 'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return new AspectRatio(
        aspectRatio: 100 / 100,
        child: new Scaffold(
            appBar: new AppBar(
              elevation: 0.0,
              backgroundColor: greyApp.withOpacity(0),
              title: new Center(
                  child: new Container(
                      padding: EdgeInsets.only(right: 60.0),
                      child: new Text('Quick Diagnostic',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 23)))),
            ),
            backgroundColor: greyApp,
            body: Theme(
                data: ThemeData(
                  primaryColor: greenApp,
                  hintColor: greenApp,
                  textTheme: TextTheme(
                    subhead: TextStyle(color: Colors.white),
                  ),
                  scaffoldBackgroundColor: greyApp,
                  canvasColor: greyApp,
                  backgroundColor: greyApp.withOpacity(0),
                ),
                child: new Container(
                  padding: new EdgeInsets.all(32.0),
                  child: new Center(
                    child: new Column(
                      children: <Widget>[
                        new Row(
                          children: <Widget>[
                            new Container(
                              padding: new EdgeInsets.all(5.0),
                            ),
                          ],
                        ),
                        new GestureDetector(
                          onTap: () {
                            _scan();
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.height / 3.2,
                            height: MediaQuery.of(context).size.height / 3.2,
                            child: FloatingActionButton.extended(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(
                                          MediaQuery.of(context).size.height /
                                              3.2)),
                                  side: BorderSide(
                                      color: greyCircle, width: 15.0)),
                              onPressed: null,
                              elevation: 10,
                              tooltip: 'Scan barcode',
                              label: AutoSizeText('SCAN BARCODE',
                                  style: new TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12)),
                              backgroundColor: greenCircle,
                              icon: Icon(LineIcons.barcode,
                                  size: 100, color: Colors.white),
                            ),
                          ),
                        ),
                        new Padding(padding: new EdgeInsets.all(15.0)),
                        Container(
                          child: AutoSizeText(barcode,
                              style: new TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15)),
                        ),
                        new Padding(padding: new EdgeInsets.all(16.0)),
                        ConstrainedBox(
                          constraints: BoxConstraints.expand(
                              height: MediaQuery.of(context).size.height / 3.2),
                          child: new Card(
                            color: greyApp,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: new Container(
                              decoration: BoxDecoration(
                                  color: greenApp,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(18))),
                              child: new Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  new Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      new Container(
                                        padding: EdgeInsets.all(10.0),
                                        child: AutoSizeText('GSM Status',
                                            style: new TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13)),
                                      ),
                                      new Container(
                                          padding: EdgeInsets.all(10.0),
                                          child: new CircleAvatar(
                                              backgroundColor: greyApp,
                                              child: new LayoutBuilder(builder:
                                                  (context, constraint) {
                                                return new Icon(Icons.check,
                                                    size: constraint
                                                            .biggest.height /
                                                        2,
                                                    color: Colors.white);
                                              })))
                                    ],
                                  ),
                                  Divider(
                                    color: Colors.white,
                                  ),
                                  new Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      new Container(
                                        padding: EdgeInsets.all(10.0),
                                        child: Text('GPS Status',
                                            style: new TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13)),
                                      ),
                                      new Container(
                                        padding: EdgeInsets.all(10.0),
                                        child: new CircleAvatar(
                                            backgroundColor: greyApp,
                                            child: new LayoutBuilder(
                                                builder: (context, constraint) {
                                              return new Icon(Icons.close,
                                                  size: constraint
                                                          .biggest.height /
                                                      2,
                                                  color: Colors.white);
                                            })),
                                      )
                                    ],
                                  ),
                                  Divider(
                                    color: Colors.white,
                                  ),
                                  new Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      new Container(
                                        padding: EdgeInsets.all(10.0),
                                        child: Text('Number of SUR',
                                            style: new TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13)),
                                      ),
                                      new Container(
                                        padding: EdgeInsets.all(10.0),
                                        child: new CircleAvatar(
                                          backgroundColor: greyApp,
                                          child: new AutoSizeText(
                                            '3',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ))));
  }
}
