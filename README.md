# Assistance Mobile Application (AMA) 

AMA permettra de fournir assistance aux clients lors de la mise en service de nouveaux équipements. Elle facilitera la saisie d'information en utilisant des technologies de reconnaissance d'image pour scanner des plaquettes signalétiques et elle informera en temps réel du bon fonctionnement aussi bien du réseau électrique que de l'équipement Depsys. Cette application communiquera avec une plateforme IoT pour échanger les informations avec les serveurs centralisés de Depsys.

## Prérequis
* MacOs / Windows 10 / Linux
* [Flutter](https://flutter.dev/)
* Android Studio ou VSCode
* Dispositif Android / iOS mobile physique ou émulateur


## Installation
* Cloner ou télécharger le projet
```
git clone https://gitlab.forge.hefr.ch/nestor.penalope/ps6-penalopez.git
```

* Pour avoir exactement le même comportement, veuillez copier le dossier 13. Flutter version/lib dans le dossier flutter/packages/flutter de votre machine

* Importer toutes les dépendances
```
flutter get packages
```

* Si des comportements inattendus surviennent pendant l'installation veuillez vous rendre [ici](https://flutter.dev/docs/development/packages-and-plugins/androidx-compatibility)

## Conçue avec

* [Flutter](https://flutter.dev/)


## Auteur

* **Nestor Pena Lopez** - [HomePage](http://nestor.penalope.home.hefr.ch/about.html)


## Superviseurs

* **Nicolas Schroeter** 
* **Jacques Supcik**

## Mandant

* **Simon Reynaud** - [DEPsys SA](https://www.depsys.ch/)
